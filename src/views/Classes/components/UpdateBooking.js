import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {Calendar} from '../../../globalcomponents';

const UpdateBooking = (props) => {

	const classSchedule = props.classSchedule;
	const [dropdownOpen, setDropdownOpen] = useState(false)
	const [timeSlot, setTimeSlot] = useState("Time Slot")

	const toggle = () => setDropdownOpen(!dropdownOpen)

	const handleCompleteBooking = () => {
		props.handleUpdateBooking();
		props.handleShowUpdateClassForm();
	}

	const handleTimeDropdown = (e) => {
		props.handleTimeInput(e.target.innerHTML)
		setTimeSlot(e.target.innerHTML)
	}

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showUpdateForm}
				toggle={props.handleShowUpdateClassForm}
			>
				<ModalHeader
					toggle={props.handleShowUpdateClassForm}
				>
					Update Class Details
				</ModalHeader>
				<ModalBody
					className="d-flex justify-content-center align-items-center flex-column"
				>
			        <h1>{classSchedule.language}</h1>
			        <Calendar 
			        	handleDateInput={props.handleDateInput}
			        />
			        <Dropdown
						isOpen={dropdownOpen}
						toggle={toggle}
						className="py-3"
					>
						<DropdownToggle caret className="dropdown-text">
							{timeSlot==="Time Slot" ? classSchedule.time : timeSlot}
						</DropdownToggle>
						<DropdownMenu>
							<DropdownItem
								onClick={handleTimeDropdown}
								// defaultValue={"8 AM to 10 AM"}
							>8 AM to 10 AM</DropdownItem>
							<DropdownItem
								onClick={handleTimeDropdown}
							>10 AM to 12 PM</DropdownItem>
							<DropdownItem
								onClick={handleTimeDropdown}
							>1 PM to 3 PM</DropdownItem>
							<DropdownItem
								onClick={handleTimeDropdown}
							>3 PM to 5 PM</DropdownItem>
						</DropdownMenu>
					</Dropdown>
					<Button
						className="text-center btnsuccess"
						onClick={handleCompleteBooking}
					>
						Update Class
					</Button>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default UpdateBooking;