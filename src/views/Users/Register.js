import React, { useState } from 'react';
import {FormInput} from '../../globalcomponents';
import {
    Button
} from 'reactstrap';
import axios from 'axios';
import {NavigationBar, Footer} from '../Layout'

const Register = () => {

    const[firstName,setFirstName]= useState("");
    const[lastName,setLastName]= useState("");
    const[email,setEmail]= useState("");
    const[password,setPassword]=useState("");

    const handleFirstNameChange = (e) => {
        setFirstName(e.target.value)
        console.log(e.target.value)
    }

    const handleLastNameChange = (e) => {
        setLastName(e.target.value)
        console.log(e.target.value)
    }

    const handleEmailChange = (e) => {
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleRegister = () => {
        axios.post('https://fast-ocean-11047.herokuapp.com/register',{
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password
        }).then(res=>console.log(res.data))

        window.location.replace('/');
    }

    return (
        <React.Fragment>
            <NavigationBar />
            <div className="min-vh-100">
            <h1 className="text-center pt-5 header-text">Register</h1>
            <div className="col-lg-4 offset-lg-4 pb-3">
                <FormInput
                    label={"First Name"}
                    placeholder={"Enter your first name"}
                    type={"text"}
                    onChange={handleFirstNameChange}
                />
                <FormInput
                    label={"Last Name"}
                    placeholder={"Enter your last name"}
                    type={"text"}
                    onChange={handleLastNameChange}
                />
                <FormInput
                    label={"Email"}
                    placeholder={"Enter Your Email"}
                    type={"email"}
                    onChange={handleEmailChange}
                />
                <FormInput
                    label={"Password"}
                    placeholder={"Enter Your Password"}
                    type={"password"}
                    onChange={handlePasswordChange}
                />
                <Button
                    block
                    className="btnsuccess"
                    onClick={handleRegister}
                >
                    Register
                </Button>
            </div>
            </div>
            <Footer />
        </React.Fragment>
    );
}
export default Register;