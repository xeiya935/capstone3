import React from 'react';

const Footer = () => {
	return(
		<React.Fragment>
			<div className="footer-color footer-text sticky-bottom">
				<p className="text-center">Created by: Arvin Lacdao - Batch 46</p>
			</div>
		</React.Fragment>
	)
}

export default Footer;