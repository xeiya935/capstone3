import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
} from 'reactstrap'

const NavigationBar = () => {

	const [isOpen, setIsOpen] = useState(false);
	const toggle = () => setIsOpen(!isOpen);

	const logout = () =>{
		sessionStorage.token = "";
	}

	const [user,setUser] = useState("")

	if (sessionStorage.token) {
		let user = JSON.parse(sessionStorage.user)
	}
	

	return(
		<React.Fragment>
			<Navbar expand="md" className="navbar-color">
				<NavbarBrand href="/" className="logo-text">Codebase</NavbarBrand>
				<NavbarToggler onClick={toggle}/>
				<Collapse isOpen={isOpen} navbar className="navbar-toggler">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active my-auto mx-2">
							<Link to="/" className="navbar-text">Home</Link>
						</li>
						<li className="nav-item my-auto mx-2">
							<Link to="/courses" className="navbar-text">Courses</Link>
						</li>
						{sessionStorage.token ?
						<React.Fragment>
						<li className="nav-item my-auto mx-2">
							<Link to="/classes" className="navbar-text">Classes</Link>
						</li>
						<li className="nav-item my-auto mx-2">
							<Link to="/login" className="navbar-text"
							onClick={logout}
							>Logout</Link>
						</li>
						<li className="nav-item my-auto mx-2">
							<Link to="#" className="navbar-text">Hello {user.firstName}</Link>
						</li>
						</React.Fragment>
						: ""}
						{!sessionStorage.token ?
						<React.Fragment>
							<li className="nav-item my-auto mx-2">
								<Link to="/login" className="navbar-text">Login</Link>
							</li>
							<li className="nav-item my-auto mx-2">
								<Link to="/register" className="navbar-text">Register</Link>
							</li>
							<li className="nav-item my-auto mx-2">
							</li>
						</React.Fragment>
						: ""}
							
						
					</ul>
				</Collapse>
			</Navbar>
		</React.Fragment>
	)
}

export default NavigationBar;