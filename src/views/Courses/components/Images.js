import React, {useState} from 'react';
import {
  Button
} from 'reactstrap';
import axios from 'axios'
import {NavigationBar, Footer} from '../../Layout'

const Images = props => {
  const [imageToSave, setImage] = useState([]);
  const [infoMessage, setMessage] = useState("");
  const [imageUrl, setImageUrl] = useState([])

  const selectImage = e => {
    let image = [e.target.files.item(0)];

    image = image.filter(image=>image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/))
    let imagepath = e.target.files.item(0).name
    let regex = new RegExp('[^.]+$');
    let extension = imagepath.match(regex)[0];

    if(extension === "jpg" || extension === "JPG" || extension === "JPEG" || extension === "jpeg" || extension === "png" || extension === "gif"){
      let message = "Valid image selected";
      setMessage(message);
    } else {
      let message = "Invalid image selected";
      setMessage(message);
    }

    setImage(image);
  }

  const uploadImage = () => {
    let url = [...imageUrl]
    const uploaders = imageToSave.map(image=>{
      const data = new FormData();
      data.append("image", image, image.name)

      return axios.post('https://fast-ocean-11047.herokuapp.com/upload', data).then(res=>{
        url.push(res.data.imageUrl)
        setImageUrl(url)
        console.log(url)
      })
    })
  }

  return (
    <React.Fragment>
      <NavigationBar />
      <div className="col-lg-6 offset-lg-3">
        <input 
          className="form-control" 
          type="file" 
          onChange={selectImage}
        />
        <small>{infoMessage}</small>
        <Button
          block
          color="primary"
          className="py-2"
          onClick={uploadImage}
          disabled={infoMessage === "Valid image selected" ? false : true}
        >
          Upload Image
        </Button>
        <div>
          {
            imageUrl.map((url, index)=>(
              <div key={index}>
                <img 
                  src={"https://fast-ocean-11047.herokuapp.com/"+url}
                  alt="Not available"
                />
              </div>
            ))
          }
        </div>
      </div>
      <Footer />
    </React.Fragment>
  )
}

export default Images;