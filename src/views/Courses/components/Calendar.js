import React from 'react';
import DayPicker from 'react-day-picker';

const Calendar = (props) => {

	const handleDayClick = (day) => {
		props.handleDateInput(day)
	}

	const disabledDays = {
		daysOfWeek: [0,6]
	}

	const past = {
		before: new Date()
	}

	return(
		<React.Fragment>
			<div className="d-flex justify-content-center">
				<DayPicker 
					onDayClick={handleDayClick}
					showOutsideDays
					disabledDays={[past, disabledDays, new Date(2019, 10, 26)]}
				/>
			</div>
		</React.Fragment>
	)
}

export default Calendar;