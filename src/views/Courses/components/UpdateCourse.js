import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';

const UpdateCourse = (props) => {

	const course = props.course

	const handleUpdateCourse = () => {
		props.handleSaveCourse();
		props.handleRefresh();
		props.handleShowUpdateCourseForm();
	}

	const handleCancelUpdateCourse = () => {
		props.handleRefresh();
		props.handleShowUpdateCourseForm();
	}

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showUpdateCourseForm}
				toggle={props.handleShowUpdateCourseForm}
			>
				<ModalHeader
					toggle={props.handleShowUpdateCourseForm}
				>
					Update course details
				</ModalHeader>
				<ModalBody>
					<FormInput 
				        label={"Language"}
				        type={"string"}
				        name={"language"}
				        placeholder={"Enter course name"}
				        onBlur={(e)=>props.handleLanguageChange(e)}
				        // defaultValue={course.language}
			        />
			        <FormInput 
				        label={"Course Image"}
				        type={"file"}
				        name={"courseImage"}
			        />
			        <small>{props.infoMessage}</small>
			        <FormInput 
				        label={"Description"}
				        type={"textarea"}
				        name={"description"}
				        placeholder={"Enter course description here"}
				        onBlur={(e)=>props.handleDescriptionChange(e)}
				        // defaultValue={course.description}
			        />
			        <FormInput 
				        label={"Price (PHP)"}
				        type={"number"}
				        name={"price"}
				        placeholder={"Enter course price here"}
				        onBlur={(e)=>props.handlePriceChange(e)}
			        />
			        <div className="d-flex justify-content-center">
						<Button
							className="mx-1 btnsuccess"
							onClick={(e)=>handleUpdateCourse(e)}
						>
							Update Course
						</Button>
					<Button
						className="mx-1 btndanger"
						onClick={handleCancelUpdateCourse}
					>Cancel</Button>
					</div>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default UpdateCourse;