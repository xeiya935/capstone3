import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {CourseCard, AddCourse} from './components';
import {Button} from 'reactstrap';
import moment from 'moment';
import {NavigationBar, Footer} from '../Layout'
import Image from 'react-bootstrap/Image'

const Courses = () => {

	const [courses, setCourses] = useState([]);
	const [showCourseForm, setShowCourseForm] = useState(false);
	const [time, setTime] = useState("");
	const [date, setDate] = useState("");
	const [course, setCourse] = useState("");
	const [language, setLanguage] = useState("");
	const [description, setDescription] = useState("");
	const [courseImage, setCourseImage] = useState("");
	const [price,setPrice] = useState(0);
	const [showUpdateCourseForm, setShowUpdateCourseForm] = useState(false);

	const handleRefresh = () => {
		setTime("");
		setDate("");
		setCourse("");
		setLanguage("");
		setCourseImage("");
		setDescription("");
		setPrice(0);
	}

	const handleShowCourseForm = () => {
		setShowCourseForm(!showCourseForm);
	}

	const handleShowUpdateCourseForm = () => {
		setShowUpdateCourseForm(!showUpdateCourseForm)
	}

	const handleTimeInput = (time) => {
		setTime(time);
		console.log(time);
	}

	const handleDateInput = (date) => {
		setDate(date);
		console.log(date);
	}

	const handleCourseInput = (course) => {
		setCourse(course);
		console.log(course)
	}

	const handleDescriptionInput = (description) => {
		setDescription(description)
		console.log(description)
	}

	const handlePriceInput = (description) => {
		setPrice(description)
		console.log(description)
	}

	const [student, setStudent] = useState("")

	const handleSaveBooking = () => {
		let classCode = moment(new Date).format("x");
		console.log(student)
		console.log(classCode);

		axios.post('https://fast-ocean-11047.herokuapp.com/addClass',{
			classCode: classCode,
			language: course,
			time: time,
			date: moment(date).format("MMM DD YYYY"),
			student: student
		}).then(res=>{
			handleRefresh();
			console.log(res.data)
		})
	}

	const handleSaveCourse = () => {

		axios.post('https://fast-ocean-11047.herokuapp.com/addCourse',{
			language: language,
			description: description,
			courseImage: courseImage,
			price: price,
		}).then(res=>{
			let newCourses = [...courses]
			newCourses.push(res.data)
			setCourses(newCourses)
			handleRefresh();
		})
	}



	const handleLanguageChange = (e) => {
		setLanguage(e.target.value);
		console.log(e.target.value);
	}

	const handleCourseImageChange = (e) => {
		setCourseImage(e.target.value);
		console.log(e.target.value);
	}

	const handleDescriptionChange = (e) => {
		setDescription(e.target.value);
		console.log(e.target.value);
	}

	const handlePriceChange = (e) => {
		setPrice(e.target.value);
		console.log(e.target.value);
	}

	const handleDeleteCourse = (courseId) => {
		axios.delete('https://fast-ocean-11047.herokuapp.com/deleteCourse/'+courseId).then(res=>{
			let index = courses.findIndex(course=>course._id===courseId);
			let newCourses = [...courses];
			newCourses.splice(index,1);
			setCourses(newCourses);
		})
	}

	const [imageToSave, setImageToSave] = useState([]);
	const [infoMessage, setInfoMessage] = useState("");
	const [imageUrl, setImageUrl] = useState([])

	const selectImage = (e) => {
		let image = [e.target.files.item(0)];
		console.log(image)

		image = image.filter(image=>image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/))
		let imagepath = e.target.files.item(0).name
		let regex = new RegExp('[^.]+$');
		let extension = imagepath.match(regex)[0];

		if(extension === "jpg" || extension === "JPG" || extension === "JPEG" || extension === "jpeg" || extension === "png" || extension === "gif"){
			let message = "Valid image selected";
			setInfoMessage(message);
		} else {
			let message = "Invalid image selected";
			setInfoMessage(message);
		}

		setImageToSave(image);
		uploadImage()
	}

	const uploadImage = () => {
		let url = [...imageUrl]
		const uploaders = imageToSave.map(image=>{
			const data = new FormData();
			data.append("image", image, image.name)

			return (
					axios.post('https://fast-ocean-11047.herokuapp.com/upload', data).then(res=>{
					url.push(res.data.imageUrl)
					setImageUrl(url)
					console.log(url)
					setCourseImage(url)
				})
			)
		})
	}

	const [user,setUser] = useState("")

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			let studentName = user.firstName + " " + user.lastName
			setUser(user);
			setStudent(studentName);
		}
		axios.get('https://fast-ocean-11047.herokuapp.com/showCourses').then(res=>{
			setCourses(res.data)
		})
	}, []);

	return(
		<React.Fragment>
			<NavigationBar />
			<h1 className="text-center pt-5 header-text">Courses</h1>
			<div className="d-flex justify-content-center">
				{user.role==="Admin" ?
				<Button
					// color="success"
					className="my-3 btnsuccess"
					onClick={handleShowCourseForm}
				>Add Course</Button>
				: "" }
				<AddCourse 
					showCourseForm={showCourseForm}
					handleShowCourseForm={handleShowCourseForm}
					handleSaveCourse={handleSaveCourse}
					handleLanguageChange={handleLanguageChange}
					handleCourseImageChange={handleCourseImageChange}
					handleDescriptionChange={handleDescriptionChange}
					handlePriceChange={handlePriceChange}
					handleRefresh={handleRefresh}
					infoMessage={infoMessage}
					selectImage={selectImage}
					uploadImage={uploadImage}
				/>
				
			</div>
			<div className="container">
				<div className="row justify-content-center">
					{courses.map(course=>
						<CourseCard
							key={course._id}
							course={course}
							handleTimeInput={handleTimeInput}
							handleDateInput={handleDateInput}
							handleCourseInput={handleCourseInput}
							handleDescriptionInput = {handleDescriptionInput}
							handlePriceInput = {handlePriceInput}
							handleSaveBooking={handleSaveBooking}
							time={time}
							date={date}
							handleRefresh={handleRefresh}
							handleDeleteCourse={handleDeleteCourse}
							user={user}
							showUpdateCourseForm={showUpdateCourseForm}
							handleShowUpdateCourseForm={handleShowUpdateCourseForm}
							handleLanguageChange={handleLanguageChange}
							handleCourseImageChange={handleCourseImageChange}
							handleDescriptionChange={handleDescriptionChange}
							handlePriceChange={handlePriceChange}
						/>
					)}
				</div>
			</div>
			<Footer />
		</React.Fragment>
	)
}

export default Courses;