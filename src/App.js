import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';

const Register = React.lazy(()=>import('./views/Users/Register'));
const Login = React.lazy(()=>import('./views/Users/Login'));
const Courses = React.lazy(()=>import('./views/Courses/Courses'));
const Classes = React.lazy(()=>import('./views/Classes/Classes'));
const Images = React.lazy(()=>import('./views/Courses/components/Images'))

const loading = () => {
    return(
        <div className="bg-warning d-flex justify-content-center align-items-center">
        <h1>Loading...</h1>
    </div>
    )
}

const App = () =>{
  return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
              <Switch>
                  <Route
                      path="/"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>
                  <Route
                      path="/register"
                      exact
                      name="Register"
                      render={props => <Register {...props} />}
                  >
                  </Route>
                  <Route
                      path="/login"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>
                  <Route
                      path="/courses"
                      exact
                      name="Courses"
                      render={props => <Courses {...props} />}
                  >
                  </Route>
                  <Route
                      path="/classes"
                      exact
                      name="Classes"
                      render={props => <Classes {...props} />}
                  >
                  </Route>
                  <Route
                      path="/images"
                      exact
                      name="Images"
                      render={props => <Images {...props} />}
                  >
                  </Route>
              </Switch>
          </React.Suspense>
      </HashRouter>
  )
}

export default App;